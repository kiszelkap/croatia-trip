Gyűjtemény: https://gitlab.com/kiszelkap/croatia-trip/-/blob/main/README.md

# Opatija

Lokáció: [Google maps](https://www.google.com/maps/place/Abb%C3%A1zia,+Horv%C3%A1torsz%C3%A1g/@45.3395259,14.277706,13z/data=!3m1!4b1!4m5!3m4!1s0x4764a84acbb5741b:0x400ad50862bc290!8m2!3d45.3376197!4d14.305196)

## Hotelek

*Az árak páronként értendőek.*

### Hotel Villa Kapetanovic

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/kapetanovic.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=29142202_123451821_0_0_0%2C29142202_123451821_0_0_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-90715&dest_type=city&dist=0&from_beach_key_ufi_sr=1&group_adults=4&group_children=0&hapos=12&highlighted_blocks=29142202_123451821_0_0_0%2C29142202_123451821_0_0_0&hpos=12&nflt=roomfacility%3D11%3Broomfacility%3D38%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627114537&srpvid=65943a14a30600ce&type=total&ucfs=1&activeTab=main "Hotel Villa Kapetanovic")

Besorolás: ****

#### Jellemzők

Két szobatípus elérhető:
- Standard szoba kétszemélyes ággyal - medencére néző kilátással: **216.820 HUF** (240.315 HUF reggelivel)
- Superior szoba kétszemélyes ággyal - tengerre nyíló kilátással: **263.525 HUF** (292.205 HUF reggelivel)

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wellness
- wifi
- bár
- terasz

Ingyenes lemondás augusztus 14. 23:59 előtt.

Lokáció: [Nova cesta 12 A, 51410 Abbázia, Horvátország](https://www.google.com/maps/place/Hotel+Villa+Kapetanovic/@45.3561025,14.3191519,15z/data=!4m2!3m1!1s0x0:0xc48e78a6b7e00e61?sa=X&ved=2ahUKEwjx97nPpvvxAhWSHc0KHQRSDGIQ_BIwEHoECD8QBQ) - közel a belvároshoz

Booking értékelés: **9.1**

### Amadria Park Hotel Agava

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/agava.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=1990714_122493566_0_0_0%2C1990714_122493566_0_0_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-90715&dest_type=city&dist=0&from_beach_key_ufi_sr=1&group_adults=4&group_children=0&hapos=4&highlighted_blocks=1990714_122493566_0_0_0%2C1990714_122493566_0_0_0&hpos=4&nflt=roomfacility%3D11%3Broomfacility%3D38%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627114537&srpvid=65943a14a30600ce&type=total&ucfs=1&activeTab=main "Amadria Park Hotel Agava")

Besorolás: ****

#### Jellemzők

Három szobatípus elérhető:
- Szoba kétszemélyes ággyal vagy 2 külön ággyal - tetőtér: **225.295 HUF** (239.710 HUF reggelivel)
- Standard szoba kétszemélyes ággyal vagy 2 külön ággyal - kilátással a városra: **253.405 HUF** (270.700 HUF reggelivel)
- Standard szoba kétszemélyes ággyal vagy 2 külön ággyal - kilátással a tengerre: **262.050 HUF** (285.115 HUF reggelivel)

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- terasz
- parkolás

Ingyenes lemondás augusztus 15. 23:59 előtt.

Lokáció: [Maršala Tita 89, 51410 Abbázia, Horvátország](https://www.google.com/maps/place/Amadria+Park+Hotel+Agava/@45.3371162,14.3082399,15z/data=!4m2!3m1!1s0x0:0x6a2caee18d567368?sa=X&ved=2ahUKEwiEiIy1qPvxAhVIGs0KHb5DBIAQ_BIwDHoECDwQBQ) - közel a belvároshoz

Booking értékelés: **8.8**

### Hotel Istra

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/istra-opatija.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=2094102_273402819_0_1_0%2C2094102_273402819_0_1_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-90715&dest_type=city&dist=0&from_beach_key_ufi_sr=1&group_adults=4&group_children=0&hapos=5&highlighted_blocks=2094102_273402819_0_1_0%2C2094102_273402819_0_1_0&hpos=5&nflt=roomfacility%3D11%3Broomfacility%3D38%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627114537&srpvid=65943a14a30600ce&type=total&ucfs=1&activeTab=main "Hotel Istra")

Besorolás: ***

#### Jellemzők

Egy szobatípus elérhető:
- Szoba 2 külön ággyal: **246.235 HUF** reggelivel

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- bár
- vízpart mellett
- medence

Ingyenes lemondás augusztus 15. 23:59 előtt.

Lokáció: [Maršala Tita 143, 51410 Abbázia, Horvátország](shorturl.at/iuL89) - távolabb a belvárostól

Booking értékelés: **8.3**

### Hotel Gardenija

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/gardenija.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=304212901_124205785_0_34_0%2C304212901_124205785_0_34_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-90715&dest_type=city&dist=0&from_beach_key_ufi_sr=1&group_adults=4&group_children=0&hapos=10&highlighted_blocks=304212901_124205785_0_34_0%2C304212901_124205785_0_34_0&hpos=10&nflt=roomfacility%3D11%3Broomfacility%3D38%3Bht_id%3D204%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627116809&srpvid=1b3d3e84326801a1&type=total&ucfs=1&activeTab=main#map_closed "Hotel Gardenija")

Besorolás: ***

#### Jellemzők

Három szobatípus elérhető:
- Standard szoba kétszemélyes vagy 2 külön ággyal: **218.810 HUF** (236.110 HUF reggelivel, 241.870 HUF reggeli -és vacsorával)
- Standard szoba kétszemélyes ággyal vagy 2 külön ággyal - kilátással a tengerre: **227.460 HUF** (256.285 HUF reggelivel)
- Erkélyes lakosztály: **340.465 HUF** (357.465 HUF reggelivel)

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- bár
- terasz
- parkoló

Ingyenes lemondás augusztus 15. 23:59 előtt.

Lokáció: [M. Tita 83 Opatija, 51410 Abbázia, Horvátország](https://www.google.com/maps/place/Hotel+Gardenija/@45.3378551,14.3088318,15z/data=!4m2!3m1!1s0x0:0x7c90529c5f9413fe?sa=X&ved=2ahUKEwjKz6TMrPvxAhXBhv0HHcwaAzcQ_BIwGHoECEkQBQ) - közel a belvároshoz

Booking értékelés: **9.1**

### Hotel Galeb

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/galeb.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=2809301_191259600_0_41_0%2C2809301_191259600_0_41_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-90715&dest_type=city&dist=0&from_beach_key_ufi_sr=1&group_adults=4&group_children=0&hapos=20&highlighted_blocks=2809301_191259600_0_41_0%2C2809301_191259600_0_41_0&hpos=20&nflt=roomfacility%3D11%3Broomfacility%3D38%3Bht_id%3D204%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627116809&srpvid=1b3d3e84326801a1&type=total&ucfs=1&activeTab=main "Hotel Galeb")

Besorolás: ***

#### Jellemzők

Három szobatípus elérhető:
- Standard szoba kétszemélyes ággyal: **240.720 HUF** reggelivel
- Standard szoba kétszemélyes ággyal - tengerre nyíló kilátással: **272.430 HUF** reggelivel
- Erkélyes lakosztály: **340.465 HUF** (357.465 HUF reggelivel)

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- bár
- terasz
- parkoló
- medence

Ingyenes lemondás augusztus 15. 23:59 előtt.

Lokáció: [M.Tita 160, 51410 Abbázia, Horvátország](https://www.google.com/maps/place/Hotel+Galeb/@45.3311604,14.3021258,15z/data=!4m2!3m1!1s0x0:0xb1cf71f13f4f099?sa=X&ved=2ahUKEwjozc7ArfvxAhWBXc0KHUXDBN4Q_BIwDXoECD4QBQ) - közel a belvároshoz

Booking értékelés: **8.7**

## Apartmanok

*Az árak két párra összesen értendőek.*

### Apartment Minka

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/apartment-minka.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=234322101_100834104_6_0_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-90715&dest_type=city&dist=0&from_beach_key_ufi_sr=1&group_adults=4&group_children=0&hapos=5&highlighted_blocks=234322101_100834104_6_0_0&hpos=5&nflt=roomfacility%3D11%3Broomfacility%3D38%3Btdb%3D3%3Bht_id%3D201%3Bht_id%3D216%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627118434&srpvid=32c941b1c9cb008a&type=total&ucfs=1&activeTab=main#map_closed "Apartment Minka")

Besorolás: ***

#### Jellemzők

Egy apartmantípus elérhető:
- Erkélyes apartman (két hálószoba franciaággyal, egy nappali): **288.285 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- parkoló
- terasz
- mosogatógép
- 80 nm

Alapvető igényeken aluli kiemelendő tulajdonságok:
- egy fürdőszoba

Előrefizetés, nincs lemondás.

Lokáció: [67 Nova cesta, 51410 Abbázia, Horvátország](https://www.google.com/maps/place/Nova+cesta+67,+51410,+Opatija,+Horv%C3%A1torsz%C3%A1g/@45.3413702,14.3074625,19.5z/data=!4m5!3m4!1s0x4764a87c1e749dff:0xff8c9557ef4a239b!8m2!3d45.3413148!4d14.3075412) - közel a belvároshoz

Booking értékelés: **9.1**

### Apartman Mira

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/apartman-mira-pobri.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=745630801_330441833_5_0_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-90715&dest_type=city&dist=0&from_beach_key_ufi_sr=1&group_adults=4&group_children=0&hapos=10&highlighted_blocks=745630801_330441833_5_0_0&hpos=10&nflt=roomfacility%3D11%3Broomfacility%3D38%3Btdb%3D3%3Bht_id%3D201%3Bht_id%3D216%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&spdest=ci%2F-90715&sr_order=popularity&srepoch=1627118434&srpvid=32c941b1c9cb008a&type=total&ucfs=1&activeTab=main#map_closed "Apartman Mira")

Besorolás: ?

#### Jellemzők

Egy apartmantípus elérhető:
- Két hálószobás apartman (két hálószoba franciaággyal): **100.900 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- terasz
- mosogatógép

Előrefizetés, nincs lemondás.

Lokáció: [Dragi, 51211 Volosko, Horvátország](https://www.google.com/maps/place/P.A.USLUGE+D.O.O./@45.3576489,14.3094711,15z/data=!4m2!3m1!1s0x0:0x448cacb9986b4b01?sa=X&ved=2ahUKEwjkrZOhs_vxAhUFH80KHZ9kBYYQ_BIwEnoECDwQBQ) - távol a belvárostól

Booking értékelés: **10.0**

### Apartments Vesna

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/apartments-vesna-opatija.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=177677801_295778002_5_0_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-90715&dest_type=city&dist=0&from_beach_key_ufi_sr=1&group_adults=4&group_children=0&hapos=4&highlighted_blocks=177677801_295778002_5_0_0&hpos=4&nflt=roomfacility%3D11%3Broomfacility%3D38%3Btdb%3D3%3Bht_id%3D201%3Bht_id%3D216%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627118434&srpvid=32c941b1c9cb008a&type=total&ucfs=1&activeTab=main "Apartments Vesna")

Besorolás: ****

#### Jellemzők

Egy apartmantípus elérhető:
- Apartman 3 hálószobával (két hálószoba franciaággyal, egy hálószoba futonággyal): **185.320 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- terasz
- parkolás
- mosogatógép
- 90 nm

Előrefizetés, nincs lemondás.

Lokáció: [Evićev put 6, 51410, Opatija, Horvátország](https://www.google.com/maps/place/Apartman+Opatija/@45.352542,14.3099234,18z/data=!4m5!3m4!1s0x4764a88453a3272d:0x2a4fcf7e6dea4c05!8m2!3d45.3517709!4d14.3099283) - távol a belvárostól

Booking értékelés: **9.6**
