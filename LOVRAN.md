Gyűjtemény: https://gitlab.com/kiszelkap/croatia-trip/-/blob/main/README.md

# Lovran

Lokáció: [Google maps](https://www.google.com/maps/place/Lovran,+Horv%C3%A1torsz%C3%A1g/data=!4m2!3m1!1s0x476357c693170185:0x7d2de93783347752?sa=X&ved=2ahUKEwjZnIOer_vxAhWBdc0KHQ4lAVEQ8gEwE3oECEEQAQ)

## Hotelek

*Az árak páronként értendőek.*

### Hotel Lovran

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/hotellovran.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=1975302_270996512_0_1_0%2C1975302_270996512_0_1_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-90715&dest_type=city&dist=0&from_beach_key_ufi_sr=1&group_adults=4&group_children=0&hapos=27&highlighted_blocks=1975302_270996512_0_1_0%2C1975302_270996512_0_1_0&hpos=2&nflt=roomfacility%3D11%3Broomfacility%3D38%3Bht_id%3D204%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&spdest=ci%2F-90715&sr_order=popularity&srepoch=1627117339&srpvid=69a43f8d39a1002e&type=total&ucfs=1&activeTab=main "Hotel Lovran")

Besorolás: ***

#### Jellemzők

Egy szobatípus elérhető:
- Standard szoba kétszemélyes ággyal - kilátással az utcára: **182.055 HUF** reggelivel (216.650 HUF reggeli -és vacsorával)

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- bár
- terasz
- parkoló
- magánstrand
- medence

Ingyenes lemondás augusztus 16. 23:59 előtt.

Lokáció: [Setaliste Marsala Tita 19/2, 51415 Lovran, Horvátország](https://www.google.com/maps/place/Hotel+Lovran/@45.2953355,14.2762042,15z/data=!4m2!3m1!1s0x0:0x13c7f68e5c1ad68f?sa=X&ved=2ahUKEwjnr9unrvvxAhVPaM0KHXM4D08Q_BIwDXoECD4QBQ) - közel a belvároshoz

Booking értékelés: **7.7**

### Hotel Park

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/park-lovran.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=2088804_329597728_0_41_0%2C2088801_329597728_0_41_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-90715&dest_type=city&dist=0&from_beach_key_ufi_sr=1&group_adults=4&group_children=0&hapos=28&highlighted_blocks=2088804_329597728_0_41_0%2C2088801_329597728_0_41_0&hpos=3&nflt=roomfacility%3D11%3Broomfacility%3D38%3Bht_id%3D204%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&spdest=ci%2F-90715&sr_order=popularity&srepoch=1627117339&srpvid=69a43f8d39a1002e&type=total&ucfs=1&activeTab=main "Hotel Park")

Besorolás: ****

#### Jellemzők

Két szobatípus elérhető:
- Standard szoba kétszemélyes vagy 2 külön ággyal: **209.455 HUF** reggelivel (220.985 reggeli -és vacsorával)
- Standard szoba kétszemélyes vagy 2 külön ággyal (tenger felöli oldalon): **235.965 HUF** reggelivel (247.495 reggeli -és vacsorával)

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- bár
- wellness

Ingyenes lemondás augusztus 10. 23:59 előtt.

Lokáció: [M.Tita 60, 51415 Lovran, Horvátország](https://www.google.com/maps/place/Hotel+Park,+Lovran/@45.2909245,14.2767366,15z/data=!4m2!3m1!1s0x0:0xc6278c69d31923e7?sa=X&ved=2ahUKEwjxk6aTsPvxAhVGVs0KHVigBhIQ_BIwDnoECEAQBQ) - közel a belvároshoz

Booking értékelés: **8.6**

## Apartmanok

*Az árak két párra összesen értendőek.*

### Apartman Ena & Marta

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/apartman-ena-amp-marta.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=712695001_298029912_4_0_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-87543&dest_type=city&dist=0&from_beach_key_ufi_sr=1&group_adults=4&group_children=0&hapos=9&highlighted_blocks=712695001_298029912_4_0_0&hpos=9&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627119640&srpvid=8243440b8dc000ee&type=total&ucfs=1&activeTab=main#map_closed "Apartman Ena & Marta")

Besorolás: ?

#### Jellemzők

Egy apartmantípus elérhető:
- Két hálószobás apartman (két hálószoba franciaággyal, egy nappali): **131.170 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- parkoló
- mosogatógép

Alapvető igényeken aluli kiemelendő tulajdonságok:
- egy fürdőszoba
- 55 nm

Előrefizetés, ingyenes lemondás augusztus 12. 23:59 előtt.

Lokáció: [M.Tita 60, 51415 Lovran, Horvátország](https://www.google.com/maps/place/Hotel+Park,+Lovran/@45.2909245,14.2767366,15z/data=!4m2!3m1!1s0x0:0xc6278c69d31923e7?sa=X&ved=2ahUKEwjxk6aTsPvxAhVGVs0KHVigBhIQ_BIwDnoECEAQBQ) - közel a belvároshoz

Booking értékelés: **8.7**
