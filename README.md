# Horvátországi nyaralás

Igények:
- saját szoba páronként
- saját fürdőszoba páronként
- legkondícionálás
- valódi franciaágy


## Opatija

Lokáció: [Google maps](https://www.google.com/maps/place/Abb%C3%A1zia,+Horv%C3%A1torsz%C3%A1g/@45.3395259,14.277706,13z/data=!3m1!4b1!4m5!3m4!1s0x4764a84acbb5741b:0x400ad50862bc290!8m2!3d45.3376197!4d14.305196)

Szállások: https://gitlab.com/kiszelkap/croatia-trip/-/blob/main/OPATIJA.md

## Lovran

Lokáció: [Google maps](https://www.google.com/maps/place/Lovran,+Horv%C3%A1torsz%C3%A1g/data=!4m2!3m1!1s0x476357c693170185:0x7d2de93783347752?sa=X&ved=2ahUKEwjZnIOer_vxAhWBdc0KHQ4lAVEQ8gEwE3oECEEQAQ)

Szállások: https://gitlab.com/kiszelkap/croatia-trip/-/blob/main/LOVRAN.md

## Krk-sziget

Lokáció: [Google maps](https://www.google.com/maps/place/Krk/@45.0898285,14.3456242,10z/data=!3m1!4b1!4m5!3m4!1s0x47637041428ae45d:0xa2f0b329de802341!8m2!3d45.0809356!4d14.5925861)

Szállások: https://gitlab.com/kiszelkap/croatia-trip/-/blob/main/KRK.md

## Rab-sziget

Lokáció: [Google maps](https://www.google.com/maps/place/Rab,+Horv%C3%A1torsz%C3%A1g/data=!4m2!3m1!1s0x4763af420044a5bd:0xa8be0ec9117c060f?sa=X&ved=2ahUKEwjdy5ef0v3xAhWJjYsKHTXiCdMQ8gEwHnoECFMQAQ)

Szállások: https://gitlab.com/kiszelkap/croatia-trip/-/blob/main/RAB.md
