Gyűjtemény: https://gitlab.com/kiszelkap/croatia-trip/-/blob/main/README.md

# Krk-sziget

Lokáció: [Google maps](https://www.google.com/maps/place/Krk/@45.0898285,14.3456242,10z/data=!3m1!4b1!4m5!3m4!1s0x47637041428ae45d:0xa2f0b329de802341!8m2!3d45.0809356!4d14.5925861)

## Hotelek

*Az árak páronként értendőek.*

### Krk - Villa Lovorka - Hotel Resort Dražica

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/villa-lovorka-resort-drazica.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=106872405_329062630_0_1_0%2C106872405_329062630_0_1_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=2014&dest_type=region&dist=0&group_adults=4&group_children=0&hapos=5&highlighted_blocks=106872405_329062630_0_1_0%2C106872405_329062630_0_1_0&hpos=5&nflt=ht_id%3D204%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627123122&srpvid=17b54ad9764d0111&type=total&ucfs=1&activeTab=main#map_closed "Villa Lovorka - Hotel Resort Dražica")

Besorolás: ***

#### Jellemzők

Egy szobatípus elérhető:
- Standard szoba kétszemélyes ággyal: **257.110 HUF** reggelivel

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- bár
- parkoló
- vízpart mellett
- medence

Ingyenes lemondás augusztus 10. 23:59 előtt.

Lokáció: [Ružmarinska 6, 51500 Krk, Horvátország](https://www.google.com/maps/place/Villa+Lovorka+-+Hotel+Resort+Dra%C5%BEica/@45.02571,14.5813336,15z/data=!4m2!3m1!1s0x0:0xde070533daec83e2?sa=X&ved=2ahUKEwiW7fWLw_vxAhXi-yoKHcpoCWMQ_BIwF3oECE4QBQ) - közel a belvároshoz

Booking értékelés: **8.0**

### Krk - Villa Tamaris - Hotel Resort Dražica

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/tamaris-resort-drazica.hu.html?aid=304142;label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB;sid=6e253040dba40ff3ed3f6d36c5a719a7;all_sr_blocks=107341401_329062514_0_1_0%2C107341401_329062514_0_1_0;checkin=2021-08-18;checkout=2021-08-22;dest_id=2014;dest_type=region;dist=0;group_adults=4;group_children=0;hapos=6;highlighted_blocks=107341401_329062514_0_1_0%2C107341401_329062514_0_1_0;hpos=6;nflt=ht_id%3D204%3B;no_rooms=2;req_adults=4;req_children=0;room1=A%2CA;room2=A%2CA;sb_price_type=total;sr_order=popularity;srepoch=1627123122;srpvid=17b54ad9764d0111;type=total;ucfs=1&#map_closed "Villa Tamaris - Hotel Resort Dražica")

Besorolás: ***

#### Jellemzők

Egy szobatípus elérhető:
- Szoba kétszemélyes ággyal - kilátással a tengerre: **282.590 HUF** reggelivel (303.980 HUF reggeli -és vacsorával) - *az árak páronként értendőek*

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- bár
- parkoló
- vízpart mellett

Ingyenes lemondás augusztus 10. 23:59 előtt.

Lokáció: [Ružmarinska 6, 51500 Krk, Horvátország](https://www.google.com/maps/place/Tamaris+-+Hotel+Resort+Dra%C5%BEica/@45.0253181,14.5825233,15z/data=!4m2!3m1!1s0x0:0x1abc6af02b16baf1?sa=X&ved=2ahUKEwj66vnrw_vxAhUOgP0HHfejCfQQ_BIwDXoECEEQBQ) - közel a belvároshoz

Booking értékelés: **7.6**

## Apartmanok

*Az árak két párra összesen értendőek.*

### Baska - Pension Antonia

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/pansion-antonia.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=4807601_155860371_0_33_0%2C4807601_155860371_0_33_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=2014&dest_type=region&dist=0&group_adults=4&group_children=0&hapos=2&highlighted_blocks=4807601_155860371_0_33_0%2C4807601_155860371_0_33_0&hpos=2&nflt=min_bathrooms%3D2%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627121192&srpvid=64994713508900a6&type=total&ucfs=1&activeTab=main#map_closed "Pension Antonia")

Besorolás: ***

#### Jellemzők

Egy apartmantípus elérhető:
- Szoba kétszemélyes ággyal - erkéllyel: **129.730 HUF** reggelivel (193.150 HUF reggeli -és vacsorával)

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- parkoló
- bár
- panoráma

Ingyenes lemondás augusztus 13. 23:59 előtt.

Lokáció: [Jurandvor 223, 51523 Baška, Horvátország](https://www.google.com/maps/place/Pension+Antonia/@44.9759434,14.7398895,15z/data=!4m2!3m1!1s0x0:0x8ae0dff4f4fabc0b?sa=X&ved=2ahUKEwjTk8KYvPvxAhULgf0HHdyEB7wQ_BIwDHoECDsQBQ) - közel a belvároshoz

Booking értékelés: **8.3**

### Punat - Apartment Natali Punat

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/apartment-natali-punat-free-parking-on-premises.hu.html?aid=304142;label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB;sid=6e253040dba40ff3ed3f6d36c5a719a7;all_sr_blocks=750895301_331507375_6_0_0;checkin=2021-08-18;checkout=2021-08-22;dest_id=-74571;dest_type=city;dist=0;from_beach_key_ufi_sr=1;group_adults=4;group_children=0;hapos=4;highlighted_blocks=750895301_331507375_6_0_0;hpos=4;nflt=roomfacility%3D11%3Bht_id%3D201%3Bht_id%3D216%3Btdb%3D3%3Bmin_bathrooms%3D2%3B;no_rooms=2;req_adults=4;req_children=0;room1=A%2CA;room2=A%2CA;sb_price_type=total;spdest=ci%2F-74571;sr_order=popularity;srepoch=1627121689;srpvid=ae19480cd4f600fe;type=total;ucfs=1&#hotelTmpl "Apartment Natali Punat")

Besorolás: ?

#### Jellemzők

Egy apartmantípus elérhető:
- Apartman 3 hálószobával: **360.355 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- parkoló
- mosógép
- terasz

Ingyenes lemondás augusztus 3. 23:59 előtt.

Lokáció: [Pelinković 6, 51521 Punat, Horvátország](https://www.google.com/maps/place/51521,+Punat,+Horv%C3%A1torsz%C3%A1g/@45.0201885,14.6349447,19z/data=!3m1!4b1!4m13!1m7!3m6!1s0x476373bff3bae891:0x829de145e44fc3f7!2zUHVuYXQsIEhvcnbDoXRvcnN6w6Fn!3b1!8m2!3d45.020739!4d14.6315962!3m4!1s0x476373ea0e6c484d:0xd149485aa8c232aa!8m2!3d45.020187!4d14.635475) - közel a belvároshoz

Booking értékelés: ?

### Punat - Apartment Vazmica

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/apartment-vazmica.hu.html?aid=304142&label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=232873202_295779125_4_0_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=-85918&dest_type=city&dist=0&from_beach_non_key_ufi_sr=1&group_adults=4&group_children=0&hapos=8&highlighted_blocks=232873202_295779125_4_0_0&hpos=8&nflt=ht_id%3D201%3Btdb%3D3%3Bht_id%3D216%3Bmin_bathrooms%3D2%3Broomfacility%3D11%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&spdest=ci%2F-85918&sr_order=popularity&srepoch=1627122272&srpvid=81d7492fa1b40136&type=total&ucfs=1&activeTab=main#map_closed "Apartment Vazmica")

Besorolás: ***

#### Jellemzők

Egy apartmantípus elérhető:
- Apartman 3 hálószobával (két franciaágyas szoba, egy egyszemélyes ágyas szoba, nappali): **305.345 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- parkoló
- mosógép
- mosogatógép
- terasz

Előrefizetés, nincs lemondás.

Lokáció: [51521 Punat, Horvátország](https://www.google.com/maps/place/Ul.+Ru%C4%91era+Bo%C5%A1kovi%C4%87a+105-95,+51521,+Punat,+Horv%C3%A1torsz%C3%A1g/@45.0159193,14.6303169,17z/data=!3m1!4b1!4m5!3m4!1s0x476373bfe27d3dd1:0xf5768d947ce4a7b8!8m2!3d45.0159152!4d14.6325114) - közel a belvároshoz

Booking értékelés: **9.4**

### Malinksa - Guesthouse Villa Adria

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/villa-adria-malinska.hu.html?aid=304142;label=gen173nr-1FCAEoggI46AdIM1gEaGeIAQGYARG4ARfIAQzYAQHoAQH4AQuIAgGoAgO4AtKX74cGwAIB0gIkOWY0ODkwYWItMjZhNS00YTkwLWFkYWQtMmQ3NGQ1ZWExZWNl2AIG4AIB;sid=6e253040dba40ff3ed3f6d36c5a719a7;all_sr_blocks=70821601_204343695_0_0_0%2C70821605_204343695_0_0_0;checkin=2021-08-18;checkout=2021-08-22;dest_id=-85918;dest_type=city;dist=0;from_beach_non_key_ufi_sr=1;group_adults=4;group_children=0;hapos=11;highlighted_blocks=70821601_204343695_0_0_0%2C70821605_204343695_0_0_0;hpos=11;nflt=ht_id%3D201%3Btdb%3D3%3Bht_id%3D216%3Bmin_bathrooms%3D2%3Broomfacility%3D11%3B;no_rooms=2;req_adults=4;req_children=0;room1=A%2CA;room2=A%2CA;sb_price_type=total;spdest=ci%2F-85918;sr_order=popularity;srepoch=1627122272;srpvid=81d7492fa1b40136;type=total;ucfs=1&#map_closed "Guesthouse Villa Adria")

Besorolás: ****

#### Jellemzők

Egy apartmantípus elérhető:
- 2 hálószobás családi lakosztály (két franciaágyas szoba): **194.595 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- parkoló

Ingyenes lemondás augusztus 10. 23:59 előtt.

Lokáció: [Dubašljanska 85, 51511 Malinska, Horvátország](https://www.google.com/maps/place/Apartments+Vila+DK+Malinska/@45.1249205,14.5274694,18z/data=!4m8!3m7!1s0x47636f23e33c45eb:0x56e5babf27ec301b!5m2!4m1!1i2!8m2!3d45.1253742!4d14.5282146) - közel a belvároshoz

Booking értékelés: **7.5**

### Silo - Villa Bellator

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/villa-bellator.en-gb.html?aid=898224&app_hotel_id=1436135&checkin=2021-08-18&checkout=2021-08-22&from_sn=ios&group_adults=4&group_children=0&label=Share-utuVmZ%401627141298&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA%2CA%2CA%2C&activeTab=main#map_closed "Villa Bellator")

Besorolás: ***

#### Jellemzők

Két apartmantípus elérhető:
- Stúdió kilátással a medencére - földszint (egy franciaágyas szoba): **117.840 HUF**
- Studió (egy franciaágyas szoba): **117.840 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- parkoló
- medence

Előrefizetés, nincs lemondás.

Lokáció: [Jesenovice 142, 51515 Šilo, Croatia](https://www.google.com/maps/place/Apartments+Bellator/@45.1425538,14.6607338,15z/data=!4m2!3m1!1s0x0:0x6667e1dc4b447edc?sa=X&ved=2ahUKEwjcwYTgivzxAhWIAhAIHUQAAqAQ_BIwFXoECEYQBQ) - közel a belvároshoz

Booking értékelés: **9.0**

### Krk - Apartmani Dekanić Krk

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/apartmani-dekanic-krk-krk.en-gb.html?aid=898224&app_hotel_id=7530727&checkin=2021-08-18&checkout=2021-08-22&from_sn=ios&group_adults=4&group_children=0&label=Share-tgfr1g%401627142313&no_rooms=3&req_adults=4&req_children=0&room1=A%2CA%2CA%2CA%2C "Apartmani Dekanić Krk")

Besorolás: ?

#### Jellemzők

Egy apartmantípus elérhető:
- Apartman - földszint (két franciaágyas szoba, nappali): **166.775 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- terasz
- parkoló

Előrefizetés, nincs lemondás.

Lokáció: [16 Ulica kralja Tomislava, 51500 Krk, Horvátország](https://www.google.com/maps/place/Apartments+Dekanic/@45.0267291,14.5702314,15z/data=!4m2!3m1!1s0x0:0xfb82488abb975bf6?sa=X&ved=2ahUKEwjIlJqTjPzxAhVp-ioKHVz-B2EQ_BIwFHoECEgQBQ) - közel a belvároshoz

Booking értékelés: ?
