Gyűjtemény: https://gitlab.com/kiszelkap/croatia-trip/-/blob/main/README.md

# Rab-sziget

Lokáció: [Google maps](https://www.google.com/maps/place/Rab,+Horv%C3%A1torsz%C3%A1g/data=!4m2!3m1!1s0x4763af420044a5bd:0xa8be0ec9117c060f?sa=X&ved=2ahUKEwjdy5ef0v3xAhWJjYsKHTXiCdMQ8gEwHnoECFMQAQ)

## Apartmanok

*Az árak két párra összesen értendőek.*

### Nina Villa

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/nina-villa.hu.html?aid=898224&label=Share-eD39Zh%401627163092&sid=6e253040dba40ff3ed3f6d36c5a719a7&checkin=2021-08-18&checkout=2021-08-22&dist=0&from_sn=ios&group_adults=4&group_children=0&lang=hu&no_rooms=3&req_adults=4&req_children=0&room1=A%2CA%2CA%2CA%2C&sb_price_type=total&soz=1&type=total&lang_click=other;cdl=en-gb;lang_changed=1 "Nina Villa")

Besorolás: ****

#### Jellemzők

Egy apartmantípus elérhető:
- Villa (két franciaágyas hálószoba, egy kétszemélyes ágyas hálószoba, egy nappali): **408.645 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- parkoló
- terasz
- grill
- 157 nm
- panoráma
- mosogatógép
- mosógép
- 3 fürdőszoba

Előrefizetés, nincs lemondás.

Lokáció: [Palit 391A, 51280 Rab, Horvátország](https://www.google.com/maps/place/Palit+391,+51280,+Rab,+Horv%C3%A1torsz%C3%A1g/@44.7682254,14.7506691,17.5z/data=!4m5!3m4!1s0x4763af3bfeb5bc31:0x7c0f6bed351162bf!8m2!3d44.7681983!4d14.7521257) - közel a belvároshoz

Booking értékelés: **9.4**

### NOVO! Kuća za odmor Maslina

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/rab-barbat-na-rabu.hu.html?aid=898224&label=Share-MYybxv%401627163388&sid=6e253040dba40ff3ed3f6d36c5a719a7&checkin=2021-08-18&checkout=2021-08-22&dist=0&from_sn=ios&group_adults=4&group_children=0&lang=hu&no_rooms=3&req_adults=4&req_children=0&room1=A%2CA%2CA%2CA%2C&sb_price_type=total&soz=1&type=total&lang_click=other;cdl=en-gb;lang_changed=1 "Nina Villa")

Besorolás: ?

#### Jellemzők

Egy apartmantípus elérhető:
- Két hálószobás ház (két franciaágyas hálószoba): **354.160 HUF** VAGY **393.510**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- terasz
- 75 nm
- panoráma
- mosógép
- 3 fürdőszoba

Előrefizetés, nincs lemondás.

Lokáció: Barbat, 51280 Barbat na Rabu, Horvátország - *nincsen pontos cím*

Booking értékelés: ?

### Palma Guesthouse

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/sobe-palma.hu.html?aid=898224&label=Share-MYybxv%401627163388&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=100220207_329653621_0_2_0%2C100220201_329653621_2_2_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=2460&dest_type=region&dist=0&group_adults=4&group_children=0&hapos=1&highlighted_blocks=100220207_329653621_0_2_0%2C100220201_329653621_2_2_0&hpos=1&nflt=ht_id%3D201%3Bht_id%3D216%3Bmin_bathrooms%3D2%3Broomfacility%3D11%3Btdb%3D3%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627197021&srpvid=1157326e4a64003a&type=total&ucfs=1&activeTab=main#map_closed "Palma Guesthouse")

Besorolás: ***

#### Jellemzők

Egy apartmantípus elérhető:
- Szoba kétszemélyes ággyal és terasszal: **144.145 HUF** - *az árak páronként értendőek*
- Egyszerű szoba kétszemélyes ággyal: **115.315** - *az árak páronként értendőek*

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- terasz
- parkolás
- panoráma

Ingyenes lemondás augusztus 9. 23:59 előtt.

Lokáció: [Palit 156, 51280 Rab, Horvátország](https://www.google.com/maps/place/Breakfast+Palma+(Palma+Guest+House)/@44.7429276,14.7912413,15z/data=!4m2!3m1!1s0x0:0x9015330aa836619d?sa=X&ved=2ahUKEwjq2O-O1v3xAhXjgf0HHRMkDOYQ_BIwE3oECEEQBQ) - közel a belvároshoz

Booking értékelés: **8.5**

### Guest House Anggela

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/guest-house-anggela.hu.html?aid=898224;label=Share-MYybxv%401627163388;sid=6e253040dba40ff3ed3f6d36c5a719a7;all_sr_blocks=129839803_200325272_3_2_0%2C129839805_200325272_2_2_0;checkin=2021-08-18;checkout=2021-08-22;dest_id=2460;dest_type=region;dist=0;group_adults=4;group_children=0;hapos=2;highlighted_blocks=129839803_200325272_3_2_0%2C129839805_200325272_2_2_0;hpos=2;nflt=ht_id%3D201%3Bht_id%3D216%3Bmin_bathrooms%3D2%3Broomfacility%3D11%3Btdb%3D3%3B;no_rooms=2;req_adults=4;req_children=0;room1=A%2CA;room2=A%2CA;sb_price_type=total;sr_order=popularity;srepoch=1627197021;srpvid=1157326e4a64003a;type=total;ucfs=1&#hotelTmpl "Guest House Anggela")

Besorolás: ****

#### Jellemzők

Egy apartmantípus elérhető:
- Deluxe háromágyas szoba: **147.025 HUF** (163.445 HUF reggelivel)- *az árak páronként értendőek*
- Deluxe szoba kétszemélyes ággyal, erkéllyel és kilátással a tengerre: **145.585** (160.005 HUF reggelivel) - *az árak páronként értendőek*

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- terasz
- parkolás
- panoráma

Ingyenes lemondás augusztus 3. 23:59 előtt.

Lokáció: [Supetarska Draga 216, 51280 Supetarska Draga, Horvátország](https://www.google.com/maps/place/Guest+House+Anggela/@44.8011525,14.7149738,15z/data=!4m2!3m1!1s0x0:0x2e9e29dcb9966366?sa=X&ved=2ahUKEwj_6Meq1_3xAhXuCRAIHWawCqIQ_BIwFXoECEIQBQ) - közel a belvároshoz

Booking értékelés: **8.8**

### Rooms Vilma close to Rab City

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/guest-house-vilma-rab14.hu.html?aid=898224;label=Share-MYybxv%401627163388;sid=6e253040dba40ff3ed3f6d36c5a719a7;all_sr_blocks=135862702_238740681_2_0_0%2C135862702_238740681_2_0_0;checkin=2021-08-18;checkout=2021-08-22;dest_id=2460;dest_type=region;dist=0;group_adults=4;group_children=0;hapos=3;highlighted_blocks=135862702_238740681_2_0_0%2C135862702_238740681_2_0_0;hpos=3;nflt=ht_id%3D201%3Bht_id%3D216%3Bmin_bathrooms%3D2%3Broomfacility%3D11%3Btdb%3D3%3B;no_rooms=2;req_adults=4;req_children=0;room1=A%2CA;room2=A%2CA;sb_price_type=total;sr_order=popularity;srepoch=1627197021;srpvid=1157326e4a64003a;type=total;ucfs=1&#hotelTmpl "Rooms Vilma close to Rab City")

Besorolás: ***

#### Jellemzők

Egy apartmantípus elérhető:
- Szoba kétszemélyes ággyal - saját fürdőszobával: **98.165 HUF** - *az árak páronként értendőek*

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- terasz
- parkolás
- grill

Ingyenes lemondás augusztus 3. 23:59 előtt.

Lokáció: [Palit 40, 51280 Rab, Horvátország](https://www.google.com/maps/place/ROOMS+VILMA+CLOSE+TO+THE+RAB+CITY/@44.7619025,14.7620179,15z/data=!4m2!3m1!1s0x0:0x8a03159ae6e3e865?sa=X&ved=2ahUKEwjtm_b41_3xAhUUhP0HHYFLAlgQ_BIwFHoECEAQBQ) - közel a belvároshoz

Booking értékelés: **9.4**

### Two-Bedroom Apartment in Rab

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/two-bedroom-apartment-in-rab.hu.html?aid=898224&label=Share-MYybxv%401627163388&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=252809302_310732036_4_0_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=2460&dest_type=region&dist=0&group_adults=4&group_children=0&hapos=11&highlighted_blocks=252809302_310732036_4_0_0&hpos=11&nflt=ht_id%3D201%3Bht_id%3D216%3Bmin_bathrooms%3D2%3Broomfacility%3D11%3Btdb%3D3%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627197021&srpvid=1157326e4a64003a&type=total&ucfs=1&activeTab=main#map_closed "Two-Bedroom Apartment in Rab")

Besorolás: ***

#### Jellemzők

Egy apartmantípus elérhető:
- Két hálószobás apartman (két franciaágyas szoba): **202.880 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- parkolás
- grill

Előrefizetés, nincs lemondás.

Lokáció: 51280 Rab, Horvátország - *nincsen pontos cím*

Booking értékelés: ?

### Apartments with a parking space Supetarska Draga - Donja, Rab - 17826

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/apartments-with-a-parking-space-supetarska-draga-donja-rab-17826.hu.html?aid=898224&label=Share-MYybxv%401627163388&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=545150502_325705461_4_0_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=2460&dest_type=region&dist=0&group_adults=4&group_children=0&hapos=12&highlighted_blocks=545150502_325705461_4_0_0&hpos=12&nflt=ht_id%3D201%3Bht_id%3D216%3Bmin_bathrooms%3D2%3Broomfacility%3D11%3Btdb%3D3%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627197021&srpvid=1157326e4a64003a&type=total&ucfs=1&activeTab=main "Apartments with a parking space Supetarska Draga - Donja, Rab - 17826")

Besorolás: ***

#### Jellemzők

Egy apartmantípus elérhető:
- 2 hálószobás apartman terasszal és kilátással a tengerre (két franciaágyas szoba): **164.325 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- terasz
- parkolás
- panoráma
- 89 nm

Előrefizetés, nincs lemondás.

Lokáció: 51280 Rab, Horvátország] - *nincsen pontos cím*

Booking értékelés: ?

### Apartment NADA

Elérhetőség: [Booking](https://www.booking.com/hotel/hr/apartment-nada-mundanije.hu.html?aid=898224&label=Share-MYybxv%401627163388&sid=6e253040dba40ff3ed3f6d36c5a719a7&all_sr_blocks=209268102_295815634_4_0_0&checkin=2021-08-18&checkout=2021-08-22&dest_id=2460&dest_type=region&dist=0&group_adults=4&group_children=0&hapos=13&highlighted_blocks=209268102_295815634_4_0_0&hpos=13&nflt=ht_id%3D201%3Bht_id%3D216%3Bmin_bathrooms%3D2%3Broomfacility%3D11%3Btdb%3D3%3B&no_rooms=2&req_adults=4&req_children=0&room1=A%2CA&room2=A%2CA&sb_price_type=total&sr_order=popularity&srepoch=1627197021&srpvid=1157326e4a64003a&type=total&ucfs=1&activeTab=main#map_closed "Apartment NADA")

Besorolás: ***

#### Jellemzők

Egy apartmantípus elérhető:
- Két hálószobás apartman (két franciaágyas szoba, egy nappali): **136.58 HUF**

Alapvető igényeket felüli kiemelendő tulajdonságok:
- wifi
- terasz
- parkolás

Előrefizetés, nincs lemondás.

Lokáció: [Barbat 363 1, 51280, Barbat na Rabu, Horvátország](https://www.google.com/maps/place/Apartment+Nada/@44.7414723,14.7984154,15.25z/data=!4m5!3m4!1s0x0:0xa18907e854998fc8!8m2!3d44.7361295!4d14.8006515) - távol a belvárostól

Booking értékelés: ?
